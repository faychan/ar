var userAgent = window.navigator.userAgent.toLowerCase(),
	inapp = /instagram|fbav|fban|kakao|snapchat|line|micromessenger|twitter/.test(userAgent),
	ios = /iphone|ipod|ipad/.test(userAgent),
	safari = /safari/.test(userAgent),
	android = /android/.test(userAgent),
	chrome = /chrome/.test(userAgent);

if(inapp){
	if(android){
		alert("Please access through Chrome for the best experience")
	} else if(ios){
		alert("Please access through Safari for the best experience")
	}
} else {
	if(ios && !safari) {
		var location = window.location;
		window.open('x-web-search://?ar', "_self");
		window.location.href = location;
	} else if (android) {
		if(!chrome){
			window.location.href = 'googlechrome://navigate?url='+ location.href;
		}
	}
}

AFRAME.registerComponent("foo",{
	init:function() {
		var element = document.querySelector('body');
		this.marker = document.querySelector('a-marker')
		var model = document.querySelector('#animated-model');
		var hammertime = new Hammer(element);
		var pinch = new Hammer.Pinch(); // Pinch is not by default in the recognisers
		hammertime.add(pinch); // add it to the Manager instance

		hammertime.on('pan', (ev) => {
			let rotation = model.getAttribute("rotation")
			switch(ev.direction) {
				case 2:
					rotation.y = rotation.y + 4
					break;
				case 4:
					rotation.y = rotation.y - 4
					break;
				case 8:
					rotation.x = rotation.x + 4
					break;
				case 16:
					rotation.x = rotation.x - 4
					break;
				default:
					break;
			}
			model.setAttribute("rotation", rotation)
		});

	 	hammertime.on("pinch", (ev) => {
			let scale = model.getAttribute("scale").x*ev.scale;
			let newScale = {x:scale, y:scale, z:scale}
			model.setAttribute("scale", newScale);
		});

		hammertime.on("tap", (ev) => {
			window.location.href = "./vr-model.html";
		});
	}
});

AFRAME.registerComponent('move-camera', {
	init: function () {
		var rig = document.querySelector('a-entity');
		var buttons = document.querySelectorAll('button');
		const keys = Object.keys(buttons)
		for (const key of keys) {
			buttons[key].addEventListener('click', function () {
				const positions = ["-2 -1.7 7", "5 -1.7 -6", "-2 -1.7 -5", "10 -4 7", "-1 -9 2"];
				//livingroom, hallway, bathroom, stairs, and bedroom respectively
				rig.setAttribute("position", positions[key]);
			});
		}
	}
});